import pathlib
import subprocess

from loguru import logger

from src import settings

GIT_LOG_COMMAND = f'git log --pretty="format: {settings.SPLIT_LOGS_BY_WORD} %h | %s | %ai | % "'


def upsert_processor_repos(list_of_processor_repos):
    if not settings.UPDATE_REPO:
        logger.info("Will not clone or update")
        return
    for repo_url, processor_name in list_of_processor_repos:
        upsert_repo(processor=processor_name, url=repo_url)


def upsert_repo(processor: str, url: str) -> None:
    """
    Try to do git pull,
    if it fails with NotDirectoryError, clone the repo
    Any other exception will be logged and raised, causing the program to terminate.
    """
    if not settings.UPDATE_REPO:
        logger.debug("Will not clone or update")
        return
    try:
        git_pull(processor)
    except NotADirectoryError:
        logger.info(f"git pull failed, trying to clone {processor}")
        clone_repo_with_path(processor, url)
    except Exception as e:
        logger.error(f"ERROR when trying to upsert {processor}: {e}")
        raise


def git_pull(processor: str) -> None:
    path = settings.tmp_dir_path / processor
    _run_subprocess(command=f"git checkout {git_branch_main_or_master(processor)}", path=path)
    _run_subprocess(command="git pull", path=path)
    logger.info(f"Updated {processor} repo")


def git_branch_main_or_master(processor):
    """
    To know if master or main is used in a repo
    """
    path = settings.tmp_dir_path / processor
    result_of_command = _run_subprocess(command=f"git branch", path=path)
    if "master" in result_of_command.stdout:
        return "master"
    elif "main" in result_of_command.stdout:
        return "main"
    else:
        msg = f"No 'master' or 'main' branch in {result_of_command.stdout}"
        logger.error(msg)
        raise ValueError(msg)


def clone_repo_with_path(processor: str, url: str) -> None:
    path = settings.tmp_dir_path / processor
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)
    _run_subprocess(command=f"git clone {url} {path}", path=path)
    logger.info(f"Cloned {processor} repo")


def collect_git_logs_from_all_processors(list_of_processors: list) -> dict:
    logs_result = dict()
    for url, processor in list_of_processors:
        try:
            logs = collect_git_logs_from_processor(processor=processor)
            logs_result[processor] = logs
        except Exception as e:
            logger.error(f"Error with {processor} {e} ")
            logs_result[processor] = None
    return format_logs(logs_result)


def format_logs(all_logs: dict):
    formatted_logs = dict()
    for processor_name, logs in all_logs.items():
        if not logs:
            logger.error(f"Error: No logs for {processor_name} ")
            continue
        log_list = []
        log_posts = logs.strip().split(settings.SPLIT_LOGS_BY_WORD)
        for post in log_posts:
            if not post:
                continue
            post_parts = post.split("|")
            formatted_log = {
                "commit_hash": post_parts[0].strip(),
                "message": post_parts[1].strip(),
                "timestamp": post_parts[2].strip(),
            }
            log_list.append(formatted_log)
        formatted_logs[processor_name] = log_list
    return formatted_logs


def collect_git_logs_from_processor(processor: str):
    path = settings.tmp_dir_path / processor
    process_result = _run_subprocess(GIT_LOG_COMMAND, path)
    return process_result.stdout


def submodule_urls_from_file() -> list:
    # get submodule urls from the file .gitmodules in the pipeline repo
    with open(settings.tmp_dir_path / "pipeline" / ".gitmodules", encoding="utf8") as f:
        data = f.readlines()
    return submodule_file_parsing(data)


def submodule_file_parsing(lines_from_file) -> list:
    list_of_submodules = []
    for row in lines_from_file:
        row = row.strip()
        if row.startswith("url"):
            url = row.split()[2].strip()
            parts = row.split("/")
            name = parts[-1].split(".")[0]
            list_of_submodules.append((url, name))
    return list_of_submodules


def submodule_hashes_from_git(commit_hash_prod: str, commit_hash_prodlike: str) -> dict:
    result = dict()
    for commit_hash in [commit_hash_prod, commit_hash_prodlike]:
        path = settings.tmp_dir_path / "pipeline"
        checkout(path, commit_hash_prod)
        process_result = _run_subprocess("git submodule status", path)
        hash_dict = parse_git_submodule_status(process_result.stdout)
        result[commit_hash] = hash_dict
    checkout(path, "master")
    logger.info("completed")
    return result


def parse_git_submodule_status(result_stdout: str) -> dict:
    hash_dict = dict()
    lines = result_stdout.split("\n")
    # format: -<long commit hash>> processors/<processor-name>
    for line in lines:
        line = line.strip()
        if not line:
            continue
        hash_for_submodule = line[1:8].strip()
        processor = line.split("/")[1].strip()
        hash_dict[processor] = hash_for_submodule
    return hash_dict


def git_submodule_logs_for_commit(commit_hash: str, processor: str) -> str:
    """
    Checks out the commit you selected and get git logs up to that commit
    Restores branch to master / main at the end
    """
    path = settings.tmp_dir_path / processor
    checkout(path, commit_hash)
    git_command = GIT_LOG_COMMAND + "--submodule"
    process_result = _run_subprocess(git_command, path)
    raw_logs = process_result.stdout
    checkout(path, git_branch_main_or_master(processor))
    return raw_logs


def checkout(path, commit_hash):
    _run_subprocess(command=f"git checkout {commit_hash}", path=path)


def _run_subprocess(command: str, path):
    result = subprocess.run(command, capture_output=True, text=True, cwd=path, encoding="utf8")
    assert result.returncode == 0, f"{path}, return code: {result.returncode}"
    logger.debug(f"Executed '{command}' in '{path}'")
    return result
