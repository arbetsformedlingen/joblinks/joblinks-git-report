import os
import pathlib

root = pathlib.Path(__file__).parent.parent.absolute()
tmp_dir = "tmp"
pathlib.Path(tmp_dir).mkdir(parents=True, exist_ok=True)

tmp_dir_path = root / tmp_dir

pipeline_repo_url = "https://gitlab.com/arbetsformedlingen/joblinks/pipeline.git"

base_url = "https://gitlab.com/arbetsformedlingen/joblinks"

base_url_for_commit_hash = (
    "https://gitlab.com/arbetsformedlingen/joblinks/pipeline-gitops-for-servers/-/raw/master/bin/"
)
base_name_prod = "prod"
base_name_prod_like_onprem = "prodlike_onprem"

file_name_prod = f"run_{base_name_prod}.sh"
file_name_prodlike_onprem = f"run_{base_name_prod_like_onprem}.sh"

# This is used to get something unique for splitting logs into separate log posts
SPLIT_LOGS_BY_WORD = "SPLIT_LOGS_BY_WORD"

# True will update repos, and clone them if they do not exist locally
# set to false if you want to run quicker, but with the risk of stale data
# useful if you work with getting and formatting logs
UPDATE_REPO = os.getenv("UPDATE_REPO", "true").lower() == "true"
report_file = os.getenv("REPORT_FILE", "joblinks-git-report.md")
