import requests
from loguru import logger

from src import settings


def split_logs_into_log_posts(result_logs):
    return result_logs.split(f"| {settings.SPLIT_LOGS_BY_WORD}")


def get_commit_hash(file_name: str) -> str:
    url = settings.base_url_for_commit_hash + file_name
    r = requests.get(url)
    return parse_commit_hash_response(r.text)
    file_content = r.text.split("\n")
    for row in file_content:
        if row.startswith("PIPELINECOMMIT"):
            commit_hash = row.split("=")[1]
            logger.info(f"Pipelinecommit = {commit_hash} in {file_name} ")
            return commit_hash
    raise ValueError(f"No commit hash found in {url}")


def parse_commit_hash_response(file_content: str) -> str:
    file_content = file_content.split("\n")
    for row in file_content:
        if row.startswith("PIPELINECOMMIT"):
            commit_hash = row.split("=")[1]
            logger.info(f"Pipelinecommit = {commit_hash}  ")
            return commit_hash
    raise ValueError("No commit hash found")


def get_commit_hashes_for_environments(all_results: dict) -> dict:
    result = dict()
    prod_logs = all_results[settings.base_name_prod]
    prod_like_logs = all_results[settings.base_name_prod_like_onprem]

    # using prodlike since any new processor will show up here first
    for key, value in prod_like_logs.items():
        if isinstance(value, list) and len(value) > 0:
            prod_like = value[0]

        # get matching data from prod
        prod = prod_logs.get(key, None)
        if isinstance(prod, list) and len(prod) > 0:
            prod = prod[0]
        data = {
            settings.base_name_prod: prod,
            settings.base_name_prod_like_onprem: prod_like,
        }
        result[key] = data

    return result


def format_log(log_post: dict) -> str:
    """
    Creates a string with the values separated by pipe
    Link to the commit on Gitlab.com is in Markdown format [link-text](url)
    """
    return f"{log_post['timestamp']} | {log_post['message']} | {log_post['commit_hash']} | [link]({log_post['commit_url']})"


def add_line_to_report(message: str) -> None:
    with open(settings.report_file, mode="a", encoding="utf8") as f:
        f.write(f"{message}  \n")  # Add two spaces to create line break in Markdown


def init_report_file():
    with open(settings.report_file, mode="w") as f:
        pass


def build_commit_url(processor: str, commit_hash: str) -> str:
    return f"https://gitlab.com/arbetsformedlingen/joblinks/{processor.strip()}/-/commit/{commit_hash.strip()}"


def present_results(
        commit_hashes_from_env: dict, all_processor_logs: dict, commit_hash_prod: str, commit_hash_prodlike: str
):
    errors = set()
    init_report_file()
    for processor, logs in all_processor_logs.items():
        same_version = commit_hash_prod == commit_hash_prodlike
        try:
            processor_commit_prod = commit_hashes_from_env[commit_hash_prod][processor]
            processor_commit_prod_like = commit_hashes_from_env[commit_hash_prodlike][processor]
        except KeyError as e:
            logger.error(e)
            continue
        add_line_to_report(f"\n# {processor.upper()}")
        add_line_to_report("Repo commits (newest first):  ")
        for log_post in logs:
            """
            Assumption of age of commits:
            - repo: newest
            - prod-like: newer than prod but could be the same as prod or repo.
            - prod: oldest
            """
            log_post["commit_url"] = build_commit_url(processor, log_post["commit_hash"])
            if log_post["commit_hash"] == processor_commit_prod_like:
                if same_version:
                    add_line_to_report("## PROD & PROD-LIKE")
                    add_line_to_report(f"{format_log(log_post)}")
                    break  # since there shouldn't be anything older than prod
                else:
                    add_line_to_report(f"## PROD-LIKE: {format_log(log_post)}")

            if log_post["commit_hash"] == processor_commit_prod and not same_version:
                log_post["commit_url"] = build_commit_url(processor, log_post["commit_hash"])

                add_line_to_report(f"## PROD: {format_log(log_post)}")
                break  # since there shouldn't be anything older than prod
            add_line_to_report(f"{format_log(log_post)}")

    for err in errors:
        logger.error(f"Could not find logs for {err}")
