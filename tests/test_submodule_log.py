import pytest

from src import git_handler

lines_from_submodule_file = [
    "\n",
    '[submodule "processors/scraping"]\n',
    "\tpath = processors/scraping\n",
    "\turl = https://gitlab.com/arbetsformedlingen/joblinks/scraping.git\n",
    '[submodule "processors/job_ad_hash"]\n',
    "\tpath = processors/job_ad_hash\n",
    "\turl = https://gitlab.com/arbetsformedlingen/joblinks/job_ad_hash.git\n",
]


def test_file_content_parsing():
    result = git_handler.submodule_file_parsing(lines_from_submodule_file)
    expected_result = [
        ("https://gitlab.com/arbetsformedlingen/joblinks/scraping.git", "scraping"),
        ("https://gitlab.com/arbetsformedlingen/joblinks/job_ad_hash.git", "job_ad_hash"),
    ]
    assert result == expected_result


def test_empty():
    assert git_handler.submodule_file_parsing([]) == []


def test_sumodule_status_parser():
    result_stdout = """-ac1667e3ef07ff8b30b823ec02cdd82a8cc44535 processors/ad-patcher
-40ab06b5c7740f712e2b15eb9f3002804b408bd7 processors/add-application-deadline
-cf092a61468ce77e186ff6f4ccb9599f69f78c6f processors/add-id
-8998e4e09dd6d856dcce36c83d780dde2791adbe processors/add-jobad-enrichments-api-results
"""

    expected_hash_dict = {
        "ad-patcher": "ac1667e",
        "add-application-deadline": "40ab06b",
        "add-id": "cf092a6",
        "add-jobad-enrichments-api-results": "8998e4e",
    }
    parsed_result = git_handler.parse_git_submodule_status(result_stdout)
    assert parsed_result == expected_hash_dict
