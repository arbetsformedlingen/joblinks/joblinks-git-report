import pytest

from src import log_handler, settings

file_content = """#!/usr/bin/env bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. "$self_dir"/runfunc.sh

PIPELINECOMMIT=0ba8a28
CONF=prod.sh
PIPEGOALS_SCRAPE="data-retrieval"
PIPEGOALS_PROCESS="download-minio,aggregate_inputdata,ad-processing,integrations"
PIPEGOALS_FINISH="upload-minio,upload-elk,make-report,send-report,import-mainlog,compress-files,upload-steplogs"
SECRETS="$self_dir"/../secrets/prod/gitlab-deploy/secrets.sh
DEVOPSFLAGS="--closedsource hostdeps checkout pull build"
POSTRUN="sudo shutdown -h now"

launch
"""
expected_commit_hash = "0ba8a28"


def test_parse():
    commit_hash = log_handler.parse_commit_hash_response(file_content)
    assert commit_hash == expected_commit_hash


def test_no_commit_hash():
    """
    If "PIPELINECOMMIT=" is missing from the file, a ValueError should be raised
    """
    no_pipeline_commit = file_content.replace("PIPELINECOMMIT=", " ")
    with pytest.raises(ValueError):
        log_handler.parse_commit_hash_response(no_pipeline_commit)
