# Joblinks-git-report

Shows what versions of the different joblinks processors are running in prod and prod-like onprem.
Also shows commits to the repos that are not deployed.


## Background:

- The JobLinks pipeline is configured with a number of processors/pipeline steps as git submodules in
the [pipeline repository](https://gitlab.com/arbetsformedlingen/joblinks/pipeline).   
- Code is not deployed automatically, but by telling the Configuration Manager which commit hash of a processor should be deployed in which environment.  
- Finding what runs in production (prod) and test (prodlike) requires a couple of manual steps.  
- It has been hard to get an overview of what is deployed in the different environments, and what has been committed to the repos but not deployed.



## Example report
Example:

```
IMPORT-TO-ELASTIC
Repo commits (newest first):
2024-01-18 16:52:04 +0000 | remove renovate | b3d3766
2024-01-16 09:05:35 +0000 | Merge branch 'renovate/configure' into 'main' | c1cfac5
2024-01-16 09:05:34 +0000 | Configure Renovate | a220a47
2023-12-20 14:12:33 +0100 | remove mirror | 8e7d230
PROD & PROD-LIKE
2023-10-17 08:57:30 +0000 | Merge branch 'feature/add-https-proxy-config' into 'main' | b14000b
```


## Workflow

- Get the commit hashes for prod (`run_prod.sh`) and prodlike-onprem (`run_prodlike_onprem.sh`)
  from [files in Gitlab](https://gitlab.com/arbetsformedlingen/joblinks/pipeline-gitops-for-servers/-/tree/master/bin?ref_type=heads).
- Clone or update (if previously
  cloned) [JobLinks Pipeline repo](https://gitlab.com/arbetsformedlingen/joblinks/pipeline)
- Checkout prod commit in pipeline repo and get commit hashes for submodules
- Checkout prodlike commit in pipeline repo and get commit hashes for submodules
- Clone or update (if previously cloned) all processor
  repos ([from .gitmodules file](https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/blob/master/.gitmodules))
- Collect git logs from each processor repo
- Present the commit logs so that it will be easy to see what is running in the different environments, and to see what
  has been commited to the repo but not deployed.

## Setup:

### Installation
This project uses Poetry for dependency management, dependencies are split into:
- runtime dependencies
- development dependencies
Read more here:
  https://gitlab.com/arbetsformedlingen/documentation/-/blob/main/engineering/plattform/python-projects.md?ref_type=heads

pre-commit is used and needs to be installed and configured (`pre-commit install`)
https://gitlab.com/arbetsformedlingen/documentation/-/blob/main/engineering/plattform/pre-commit-installation-configuration.md

### Linting & formatting
`ruff` is used for linting and formatting and will run automatically (along with a few other checks) if pre-commit has been installed.

## Run the program:
`python main.py`

Reporting is done to a Markdown file with links to the commit on Gitlab.
