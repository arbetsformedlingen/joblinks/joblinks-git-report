from loguru import logger

from src import git_handler, log_handler, settings

logger.info(f"Start. Update and clone repos? {settings.UPDATE_REPO}")

commit_hash_prod = log_handler.get_commit_hash(settings.file_name_prod)
commit_hash_prod_like = log_handler.get_commit_hash(settings.file_name_prodlike_onprem)

git_handler.upsert_repo(processor="pipeline", url=settings.pipeline_repo_url)
list_of_submodule_urls = git_handler.submodule_urls_from_file()
git_handler.upsert_processor_repos(list_of_submodule_urls)

commit_hashes_from_environments = git_handler.submodule_hashes_from_git(commit_hash_prod, commit_hash_prod_like)


formatted_logs_from_all_processors = git_handler.collect_git_logs_from_all_processors(list_of_submodule_urls)

log_handler.present_results(
    commit_hashes_from_environments, formatted_logs_from_all_processors, commit_hash_prod, commit_hash_prod_like
)
logger.info("Finished")
